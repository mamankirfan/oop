<?php

require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

$sheep = new Animal("shaun");
echo "Name = " . $sheep -> name . "<br>";
echo "legs = " . $sheep -> legs . "<br>";
echo "cold_blooded = " . $sheep  -> cold_blooded . "<br><br>";

$sungokong = new Ape("kera sakti");
echo "Name = " . $sungokong -> name . "<br>";
echo "legs = " . $sungokong -> legs . "<br>";
echo "cold_blooded = " . $sungokong  -> cold_blooded . "<br>";
$sungokong->yell("Auooo <br><br>"); 

$kodok = new frog("Buduk");
echo "Name = " . $kodok -> name . "<br>";
echo "legs = " . $kodok -> legs . "<br>";
echo "cold_blooded = " . $kodok  -> cold_blooded . "<br>";
$kodok->jump("hop hop"); 

?>

